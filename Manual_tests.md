I care a lot about being able to automatically check _any_ property about my
program before it ever runs. However, some things don't have tests yet, either
because I don't know how to test them or because I've been lazy. I'll at least
record those here.

* clone this repo to a new client app, clear its save dir[1], run it, run the
  driver, add a definition:
    ```
    on.draw = function()
    end
    ```
  Hit F4. No error.
  Quit driver, quit client app. Restart client app. No error.

* clone this repo to a new client app, clear its save dir, run it, run the
  driver, add a definition that draws to screen:
    ```
    on.draw = function()
      love.graphics.print('hello!', 50,50)
    end
    ```
  Hit F4. Client app shows 'hello!'
  Quit driver, quit client app. Restart client app. No error. Client app shows 'hello!'

* clone this repo to a new client app, clear its save dir, run it, run the
  driver, add a definition containing invalid Lua:
    ```
    on.draw = function(
    ```
  Hit F4. Driver shows an error under the definition.

* clone this repo to a new client app, clear its save dir, run it, run the
  driver, add a definition containing invalid Lua:
    ```
    on.draw = function(
    ```
  Hit F4. Driver shows an error under the definition as before.
  Quit the client app. The invalid code is saved in file 0002-on.draw in the
  save dir. However, the file 'head' in the save dir contains '1'.
  Restart the client app. It loads up without error.
  Switch back to the driver. Fix the definition.
    ```
    on.draw = function()
    end
    ```
  Hit F4. The error disappears.

* clone this repo to a new client app, clear its save dir, run it, run the
  driver, add a definition containing invalid Lua:
    ```
    on.draw = function(
    ```
  Hit F4. Driver shows an error under the definition as before.
  Quit the client app. The invalid code is saved in file 0002-on.draw in the
  save dir. However, the file 'head' in the save dir contains '1'.
  [Don't restart the client app.]
  Switch back to the driver. Hit F4. The driver hangs and needs to be
  force-quit. [This is not ideal, but how things are today.]

[1] We never clear the app from the driver's config. driver.love needs to be
robust to apps changing out from under it.
