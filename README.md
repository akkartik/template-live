# Example minimal "freewheeling" app that can be modified without being restarted

[![0 dependencies!](https://0dependencies.dev/0dependencies.svg)](https://0dependencies.dev)

Running this repo in isolation won't be very helpful. If you haven't yet,
first check out [the driver app](https://codeberg.org/akkartik/driver.love).
This repo is a template you can copy to create your own live apps.

[What is a Freewheeling App?](http://akkartik.name/freewheeling)

[Some reference documentation on how to create your own apps.](reference.md)

## Invocation

Run this app from the terminal, [passing its directory to LÖVE](https://love2d.org/wiki/Getting_Started#Running_Games)

## Hacking

To modify it live without restarting the app each time, download [the driver
app](https://codeberg.org/akkartik/driver.love). An example session might look
like this (though the UI of the driver app on the left is improving over
time):

![making changes without restarting the app](assets/20221228-luaML-driver.gif)

To publish your changes:
  * delete all files with a numeric prefix from the repo, and then
  * move all files with a numeric prefix from the [save directory](https://love2d.org/wiki/love.filesystem.getSaveDirectory)
    to the repo.

## Known issues

* This approach puts one top-level definition per file, and so obscures the
  order in which definitions are loaded. In particular, initializing a global
  table to contain other global definitions will fail if you originally
  authored the other global definitions after the table.

  I never initialize global tables with other definitions. That kind of thing
  happens in `on.initialize` (akin to `love.load`; see reference.md) or
  `on.code_change`.

* Don't start the driver app while multiple freewheeling apps are running. If
  you have a driver app running with one app, shut it down before you switch
  apps. Just always open the app and then the driver. If you close the app,
  close the driver.

* Don't give up your other tools just yet. It's easy to make a mistake that
  the app doesn't recover from when you fix it. Try restarting the app, and if
  it still doesn't work, perhaps you need to fix the initial load. This isn't
  yet a tool you can keep open for months on end. (And I'm ambivalent about
  making it such a tool since your programs might stop working for others.)

* I still see driver and the app being driven occasionally crash. When I do I
  try to make things more robust. If you do you'll quite possibly crash again
  if you try to restart. In such a situation you'll have to bump down to
  editing the underlying version files by other means. See [representation.md](representation.md)
  for details of the underlying representation on disk.

* Given the above issues, both this driver and its client freewheeling app
  benefit from being launched in terminal windows rather than by being clicked
  on in a desktop OS.

## Mirrors and Forks

Updates to this repo can be downloaded from:

* https://git.sr.ht/~akkartik/template-live

Further forks are encouraged. If you show me your fork, I'll link to it here.

* https://git.sr.ht/~akkartik/night.love - an example program

## Feedback

[Most appreciated.](http://akkartik.name/contact)
